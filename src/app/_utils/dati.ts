import { Utente } from "../_models/utente";

export class DATI {

    static readonly Utenti:Utente[] =[
        
        {
            id: 1,
            nome:"Mario",
            cognome:"Rossi",
            username:"usr1",
            password:"pss",
            tessera:"Tessera 0",
            token:"ASFDGSAKLHVNIW2HT8I3TH1",
            ruoli:
            [ 
                {
                    codice: 'ROLE_USER', 
                },{
                    codice: 'ROLE_ADMIN'
                } 
            ],
        },
     
        {
            id: 2,
            nome:"Giovanni",
            cognome:"Storti",
            username:"usr2",
            password:"pss2",
            tessera:"Tessera 1",
            token:"SGSAKGJAHKHJAKL",
            ruoli: [ {codice: 'ROLE_USER'}]
        },
        {
            id: 3,
            nome:"Maria",
            cognome:"Giovanna",
            username:"ad2",
            password:"mhs",
            tessera:"Tessera 3",
            token:"gdsagfhsjsadjsjaj",
            ruoli: [ {codice: 'ROLE_USER'}, {codice:'ROLE_ADMIN'}, {codice:'ROLE_CEO'}]
        }
    ];
}
