import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Utente } from '../../_models/utente';
import { UtentiService } from '../../_services/UtentiService';
import { faCoffee } from '@fortawesome/free-solid-svg-icons'
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  
  faCoffee = faCoffee;
  public unome:string;
  public pssd:string;
  ut:Utente;
 // logged:boolean;
 // @Output('logge') slog= new EventEmitter<boolean>();

  users: Utente[];
  
  constructor(private router: Router,
              private UsrServices:UtentiService ) {
    //this.unome= ' ';
    //this.pssd= ' ';
    this.ut = new Utente();
   }

  ngOnInit() {
    
  }


  loginPass()
  {
            
       this.UsrServices.autenticate(this.unome, this.pssd).subscribe(
        u=>{ 
          this.ut=(<Utente>u);
            if(this.ut[0].id){
            //this.logged=true;
            this.UsrServices.logged=true;
            this.UsrServices.UtenteLoggeD=this.ut[0];
            /*let i:number=0;
            for (i=0; i<this.UsrServices.utentelogged.ruoli.length; i++)
            {
                if(this.UsrServices.utentelogged.ruoli[i].codice==="ROLE_ADMIN") 
                this.UsrServices.admin=true;
        
            }*/
            this.UsrServices.admin=false;
            this.ut[0].ruoli.forEach(element => {
              if(element=="ROLE_ADMIN")  this.UsrServices.admin=true;
              
            });
           // this.slog.emit(this.logged);
            this.router.navigate(['utenti'])
        }
        else this.UsrServices.logged=false;
        },
        error=>{
            console.log(error.message);
            this.UsrServices.logged=false;
        }
    )
    
      }

   /* this.UsrServices.listaUtenti('utenti').subscribe( //il subscribe permette di definire come rispondere in caso di sucesso (200) o di errore
     
   
    pippo => {
        this.users=(<Utente[]>pippo); //casting da ingresso che sarà un ogetto ottenuto da json a quello che voglio che sia, ovvero utente.
        this.users.forEach(u=> { //<--- deve stare qui altrimenti lo fa anche a a vuoto
          if(u.username==this.unome && u.password==this.pssd) 
          {
            this.logged=true;
            this.slog.emit(this.logged);
          }
        });
      },
      error => {
        console.log(error.message);
      }
    );
    
   */
  }

