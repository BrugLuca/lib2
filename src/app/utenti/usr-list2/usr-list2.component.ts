import { Component, OnInit,Output,Input,EventEmitter } from '@angular/core';
import { UtentiService } from '../../_services/UtentiService'
import { Utente } from '../../_models/utente';
import { TableModel } from '../../_models/table-model';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';


@Component({
  selector: 'app-usr-list2',
  templateUrl: './usr-list2.component.html',
  styleUrls: ['./usr-list2.component.css']
})
export class UsrList2Component implements OnInit {
  utenti:Utente[];
  semaforo:Observable<boolean>;
  notsemaforo:Observable<boolean>;
  tableTemplate: TableModel;
  selectedUsr:Utente;
 
  newUSr:Utente;
  newU:boolean;
  showDetail:boolean;

 // @Output("onSelectionUsr") selection=new EventEmitter<Utente>();
 
 // @Input("reload") rld:boolean;

  constructor(private rotta:Router,
    private UsrService:UtentiService) {
    this.tableTemplate=new TableModel(); // se non lo metto mi carica dati undefined
    this.showDetail=false;
   }

  ngOnInit() {
    this.UsrService.listaUtenti('utenti').subscribe( //il subscribe permette di definire come rispondere in caso di sucesso (200) o di errore
    pippo => {
     // if(pippo[0]){
      this.utenti=(<Utente[]>pippo); //casting da ingresso che sarà un ogetto ottenuto da json a quello che voglio che sia, ovvero utente.
      this.SetUpTable();
      this.semaforo=new Observable( (observer) => {
        observer.next(true);
        observer.complete();
     // }
      }); 
    //  } else {
   //     alert('errore');
  //    }
    },
    error => {
      console.log(error.message);
    }
  );
   //carico i dati degli array utenti dai data statici
  }
  selectUst(id:number){
    this.rotta.navigate(['utenti', id, 'edit']);
  }

  private SetUpTable()
  {
    this.tableTemplate =
    {
        columnLabels:["id", "Nome", "Cognome", "Username" ],
        dati:this.utenti,
        rowFields:["id", "nome", "cognome", "username"  ],
        idxLabel:"id"
  };

  }

  reload(event:boolean){
    
    this.UsrService.listaUtenti('utenti').subscribe( //il subscribe permette di definire come rispondere in caso di sucesso (200) o di errore
        pippo => {
        this.utenti=(<Utente[]>pippo); //casting da ingresso che sarà un ogetto ottenuto da json a quello che voglio che sia, ovvero utente.
        this.showDetail=false;
        this.SetUpTable();
        this.semaforo=new Observable( (observer) => {
          observer.next(true);
          observer.complete();
       // }
        }); 
      },
        error => {
        console.log(error.message);
        }
      );
   
  }

dettaglioUsr(id:number)
  {
    this.newU=false;
     this.UsrService.listaUtenti('utenti/' + id).subscribe( //il subscribe permette di definire come rispondere in caso di sucesso (200) o di errore
     pippo => {
        this.selectedUsr =(<Utente>pippo); //casting da ingresso che sarà un ogetto ottenuto da json a quello che voglio che sia, ovvero utente.
        if(this.selectedUsr) {
          //this.showDetail=true;
          }
    },
    error => {
      console.log(error.message);
    }
    );

   
    
  }
    newUsr()
    {
      //this.newUSr.id=0;
      this.selectedUsr=new Utente(); //ci va la parentesi perche richiamo costruttore
      this.newU=true;
      this.showDetail=true;
      this.semaforo=new Observable( (observer) => {
        observer.next(false);
        observer.complete();
     // }
      }); 
    }

}
