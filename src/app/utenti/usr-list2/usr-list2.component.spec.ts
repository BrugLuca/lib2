import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsrList2Component } from './usr-list2.component';

describe('UsrList2Component', () => {
  let component: UsrList2Component;
  let fixture: ComponentFixture<UsrList2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsrList2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsrList2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
