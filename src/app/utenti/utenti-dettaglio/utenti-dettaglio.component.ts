import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Utente } from '../../_models/utente';

import { UtentiService } from '../../_services/UtentiService';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';


@Component({
  selector: 'app-utenti-dettaglio',
  templateUrl: './utenti-dettaglio.component.html',
  styleUrls: ['./utenti-dettaglio.component.css']
})
export class UtentiDettaglioComponent implements OnInit {
  
  @Input('nuovo') nuovo:boolean;
  @Output('showUlist') shlist= new EventEmitter<boolean>();

  show:boolean;
  ad:boolean;
  usr:Utente;
  semaforo:Observable<boolean>;
  newUtente:Utente;
 



  constructor(private rottAt: ActivatedRoute,
    private rotta: Router,
    private usrService:UtentiService) { 
    this.newUtente=new Utente;
    this.ad=this.usrService.admin;
    this.usr=new Utente();
    
    
  /*  if(!this.usr.id)
    {
      this.nuovo=true;
    }*/
    
  }

  ngOnInit() {
  //  if(this.usr.id==this.usrService.UtenteLoggeD.id)      this.ad=true;
    this.rottAt.params.subscribe(
      params => {
        this.usrService.listaUtenti('utenti/'+ params.id).subscribe(
          dettaglio=>
          {
            this.usr=(<Utente>dettaglio);
            this.semaforo=new Observable( (observer) => 
            {
              observer.next(true);
              observer.complete();
            }
            );
          },
          error =>
          {
            alert("errore");
          }
        );

    

      }
    );
  }
  saveUsr(){
    this.usrService.editUsr(this.usr).subscribe(
      newu=>{
        this.usr=(<Utente>newu);
        this.rotta.navigate(['utenti']);
      },
      error => {
        console.log(error.message);
      });

  }
  newUsr(){
    
    this.newUtente.nome=this.usr.nome;
    this.newUtente.cognome=this.usr.cognome;
    this.newUtente.username=this.usr.username;
    this.newUtente.password=this.usr.password;
    this.newUtente.tessera=this.usr.tessera;
    this.usrService.newUtente(this.newUtente).subscribe(
      newu=>{
        this.newUtente=(<Utente>newu);
        this.show=true;
    this.shlist.emit(this.show);
      },
      error => {
        console.log(error.message);
      }
    );
    
    //this.newUtente=new Utente;
  }
  delete()
  {
    this.usrService.deleteUsr(this.usr).subscribe(
      u =>{
          this.usr=(<Utente>u);
          this.show=true;
          this.shlist.emit(this.show);
      },
      error =>{
        console.log(error.message);
      }
    );
    

    

  }
}
