import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { UtentiService } from '../../_services/UtentiService';
import { Utente } from '../../_models/utente';

@Component({
  selector: 'app-utenti-list',
  templateUrl: './utenti-list.component.html',
  styleUrls: ['./utenti-list.component.css']
})
export class UtentiListComponent implements OnInit {
  selectedUsr:Utente;
  utenti:Utente[];
  newUSr:Utente;
  newU:boolean;
  showDetail:boolean;
  @Output("onSelectionUsr") selection=new EventEmitter<Utente>();
 
  @Input("reload") rld:boolean;
 
  constructor(private UsrServices:UtentiService ) {
    this.newUSr = new Utente;
    this.selectedUsr=new Utente;
    this.showDetail=false;
    
   }

  ngOnInit() {
    this.UsrServices.listaUtenti('utenti').subscribe( //il subscribe permette di definire come rispondere in caso di sucesso (200) o di errore
      pippo => {
        this.utenti=(<Utente[]>pippo); //casting da ingresso che sarà un ogetto ottenuto da json a quello che voglio che sia, ovvero utente.
        
      },
      error => {
        console.log(error.message);
      }
    );
     //carico i dati degli array utenti dai data statici

  }
  reload(event:boolean){
    
          this.UsrServices.listaUtenti('utenti').subscribe( //il subscribe permette di definire come rispondere in caso di sucesso (200) o di errore
      pippo => {
        this.utenti=(<Utente[]>pippo); //casting da ingresso che sarà un ogetto ottenuto da json a quello che voglio che sia, ovvero utente.
        
      },
      error => {
        console.log(error.message);
      }
    );
    this.showDetail=false;
    
    
  }



  dettaglioUsr(id:number)
  {
    this.newU=false;
     this.UsrServices.listaUtenti('utenti/' + id).subscribe( //il subscribe permette di definire come rispondere in caso di sucesso (200) o di errore
     pippo => {
        this.selectedUsr =(<Utente>pippo); //casting da ingresso che sarà un ogetto ottenuto da json a quello che voglio che sia, ovvero utente.
        if(this.selectedUsr) {
          this.showDetail=true;
          }
    },
    error => {
      console.log(error.message);
    }
    );

   
    
  }
  newUsr()
  {
    //this.newUSr.id=0;
    this.selectedUsr=new Utente(); //ci va la parentesi perche richiamo costruttore
    this.newU=true;
    this.showDetail=true;
  }

}
