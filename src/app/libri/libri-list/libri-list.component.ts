import { Component, OnInit } from '@angular/core';
import { LibriServiceService } from '../../_services/libri-service.service';
import { Libri } from '../../_models/libri';

@Component({
  selector: 'app-libri-list',
  templateUrl: './libri-list.component.html',
  styleUrls: ['./libri-list.component.css']
})
export class LibriListComponent implements OnInit {

  libri:Libri[];
  constructor(private libService:LibriServiceService) { }

  ngOnInit() {
    this.libService.listaLibri('libri').subscribe( //il subscribe permette di definire come rispondere in caso di sucesso (200) o di errore
    pippo => {
      this.libri=(<Libri[]>pippo); //casting da ingresso che sarà un ogetto ottenuto da json a quello che voglio che sia, ovvero utente.
      
    },
    error => {
      console.log(error.message);
    }
  );
  }

}
