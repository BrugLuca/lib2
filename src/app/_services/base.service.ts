import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class BaseService {

  urlBase = 'http://localhost:3000/';

/**
 * Metodo che costruisce url completo delle chiamate api remote. L'url token da il sufisso di url che rappresenta l'end point da chiamare
 * @param {srting} url token
 * @returns {string} ****
 */



  constructor() { }

public buildMeAnUrl(urlToken:string):string {
  return this.urlBase+urlToken;
}

}
