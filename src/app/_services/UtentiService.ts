import { Injectable } from '@angular/core';
import { Utente } from '../_models/utente';

import { HttpClient } from '@angular/common/http';
import { BaseService } from './base.service';


@Injectable()
export class UtentiService extends BaseService {
    UtenteLoggeD:Utente;
    utenti:Utente[];
    admin:boolean;
    utentelogged:Utente;
    SelectedUsr:Utente;
    logged:boolean;
  constructor(private http:HttpClient) {
     super();
     this.logged=false;
  }
   public listaUtenti(quale:string) {
    const url = this.buildMeAnUrl(quale);
    return this.http.get(url);
   }
  
   public serachById(id:number) {
    return this.listaUtenti('utenti/' + id); //il subscribe permette di definire come rispondere in caso di sucesso (200) o di errore
   }

    public deleteUsr(usr:Utente){
        const url = this.buildMeAnUrl('utenti/'+ usr.id);
        return this.http.delete(url);
    }
//devo fare una PUT mettendo l'id i dati e l'url  PUT    /posts/1
    public newUtente(usr:Utente){
        const url=this.buildMeAnUrl('utenti');
        return this.http.post(url, usr);
    }
    public editUsr(usr:Utente){
        const url=this.buildMeAnUrl('utenti/'+ usr.id);
        return this.http.put(url, usr);
    }
    public autenticate(un:string,ps:string){
        const url = this.buildMeAnUrl('utenti?username='+un+'&password='+ps);
        
      return this.http.get(url);
        
    }
}
