import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BaseService } from './base.service';
import { Libri } from '../../app/_models/libri';


@Injectable({
  providedIn: 'root'
})
export class LibriServiceService extends BaseService {

  libri:Libri[];

 
constructor(private http:HttpClient) {
   super(); 
}
 public listaLibri(quale:string) {
  const url = this.buildMeAnUrl(quale);
  return this.http.get(url);
 }

 public serachBookById(id:number) {
  return this.listaLibri('libri/' + id); //il subscribe permette di definire come rispondere in caso di sucesso (200) o di errore
 }

  public deleteLib(libr:Libri){
      const url = this.buildMeAnUrl('libri/'+ libr.id);
      return this.http.delete(url);
  }

  public newLibro(libr:Libri){
      const url=this.buildMeAnUrl('libri');
      return this.http.post(url, libr);
  }

}
