import { TestBed } from '@angular/core/testing';

import { LibriServiceService } from './libri-service.service';

describe('LibriServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: LibriServiceService = TestBed.get(LibriServiceService);
    expect(service).toBeTruthy();
  });
});
