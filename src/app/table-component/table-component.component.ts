import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { TableModel } from '../_models/table-model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-table-component',
  templateUrl: './table-component.component.html',
  styleUrls: ['./table-component.component.css']
})
export class TableComponentComponent implements OnInit {

  @Input('table-model') configurazioneTabella: TableModel; //elementi di config tabella
  @Output('SelectedId') onSelectedId = new EventEmitter<any>();

  columnLabels: string []; //label colonne
  dati: Array<any>;   //array dati
  idxLabel: string;   //nome con cui chaimo il campo chiave
  rowFields:string[]; //nome proprietà degli ogg che gli passo
 
  constructor(private rotta:Router) { 
  }

  ngOnInit() {
    
    this.columnLabels=this.configurazioneTabella.columnLabels; //label colonne
    this.dati=this.configurazioneTabella.dati;   //array dati
    this.idxLabel=this.configurazioneTabella.idxLabel;   //nome con cui chaimo il campo chiave
    this.rowFields=this.configurazioneTabella.rowFields; //nome proprietà degli ogg che gli passo
  }
  SelectedId(id:number){
    this.rotta.navigate(['utenti', id, 'edit']);
   // this.onSelectedId.emit(id);
  }
}
