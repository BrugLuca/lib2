import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UsrList2Component } from '../utenti/usr-list2/usr-list2.component';
import { UtentiDettaglioComponent } from '../utenti/utenti-dettaglio/utenti-dettaglio.component';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from '../login/login/login.component';



const routes: Routes = [

  {
    path: 'login',
    component:LoginComponent,
    
  },
  {
    path: 'utenti',
    component: UsrList2Component,
    
  }, {
    path: 'utenti/:id/edit',
    component: UtentiDettaglioComponent
   
  },
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'login'
  
  },
];


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forRoot(routes, { enableTracing: true })
  ],
  exports: [
    RouterModule
  ],
})
export class RoutingModule { }
