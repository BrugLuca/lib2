import { Component, OnInit, EventEmitter,Output } from '@angular/core';

@Component({
  selector: 'app-nav-side',
  templateUrl: './nav-side.component.html',
  styleUrls: ['./nav-side.component.css']
})
export class NavSideComponent implements OnInit {
//TODO: nav bar che attiva diverse list sul component principale-> switch case come vecchio caso.
@Output('showMenu') showMenu = new EventEmitter<number>();
constructor() { }


evento(i:number){
  this.showMenu.emit(i);
}

  ngOnInit() {
  }

}
