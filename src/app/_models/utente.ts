import { Ruolo } from "./ruolo";

export class Utente {

    id:number;
    nome: string;
    cognome: string;
    tessera: string;
    username:string;
    password:string;
    token:string;
    ruoli:Ruolo[];

    constructor(){ //creato quando uso la new, per un nuovo utente assegna un id pari a -1 e un ruolo con array vuoto
        //this.id=-1;
         this.ruoli= new Array();
        }

}


