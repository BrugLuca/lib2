export class TableModel {
    
    columnLabels: string []; //label colonne
    dati: Array<any>;   //array dati
    idxLabel: string;   //nome con cui chaimo il campo chiave
    rowFields:string[]; //nome proprietà degli ogg che gli passo

}
