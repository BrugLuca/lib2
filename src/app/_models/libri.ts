import { Autore } from "./autore";

export class Libri {

    id: number;
    titolo: string;
    numPagine: number;
    autore: Autore;

}
