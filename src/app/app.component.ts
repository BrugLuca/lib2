import { Component } from '@angular/core';
import { Utente } from './_models/utente';
import { UtentiService } from './_services/UtentiService';
//import { faCoffee } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
 
  selUsr:Utente;
  title:string;
  //logged:boolean;
  mostra:boolean;
  mprest:boolean;
  mlib:boolean;
  nUst:boolean;
  rld:boolean;

  loggato:boolean;

  constructor(private usrService:UtentiService){
   
    this.selUsr= new Utente;
   // this.logged=false;
    this.mostra=true;
    this.mprest=false;
    this.mlib=false;
    this.nUst=false;
    this.loggato=this.usrService.logged;
    
  }

 
  nuovoUsr(event:boolean){
    this.nUst=event;
  }

 /* logF(event:boolean)
  {
    if (event) this.logged=true;
    
  }*/
  reload(event:boolean)
  {
    this.rld=true;
  }
  showCMenu(n:number)
  {

     switch(n){

      case (1) :{
        if (this.mostra) {
          this.mostra=false;
        } else {
          this.mostra=true;
          this.mlib=false;
          this.mprest=false;
        }
        break;
      }
      case (2):{
        if (this.mlib) {
          this.mlib=false;
        } else {
          this.mlib=true;
          this.mostra=false;
          this.mprest=false;
        
        }
        break;
      }
      case (3):{
        if (this.mprest) {
          this.mprest=false;
        } else {
          this.mprest=true;
          this.mlib=false;
          this.mostra=false;
        }
        break;

       }

      }
     }

}
