import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { HttpClientModule } from '@angular/common/http'
import { AppComponent } from './app.component';

//import { UtentiListComponent } from './utenti/utenti-list/utenti-list.component';
import { UtentiService } from './_services/UtentiService';
import { UtentiDettaglioComponent } from './utenti/utenti-dettaglio/utenti-dettaglio.component';
import { LoginComponent } from './login/login/login.component';
import { NavSideComponent } from './nav-side/nav-side.component';
import { LibriListComponent } from './libri/libri-list/libri-list.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome'
import { library } from '@fortawesome/fontawesome-svg-core'
import { faCoffee } from '@fortawesome/free-solid-svg-icons';
import { fas } from '@fortawesome/free-solid-svg-icons';
import { UsrList2Component } from './utenti/usr-list2/usr-list2.component';
import { TableComponentComponent } from './table-component/table-component.component';
import { RoutingModule } from './routing/routing.module';




@NgModule({
  declarations: [
    AppComponent,
    
    UtentiDettaglioComponent,
    LoginComponent,
    NavSideComponent,
    LibriListComponent,
    TableComponentComponent,
    UsrList2Component
  ],
  imports: [NgbModule,
    BrowserModule,
    FormsModule,
    HttpClientModule,
    FontAwesomeModule,
    RoutingModule
      ],
  providers: [UtentiService],
  bootstrap: [AppComponent]
})
export class AppModule { 
  constructor(){
  library.add(faCoffee);
  library.add(fas);
  }
}
